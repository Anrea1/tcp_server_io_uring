#include <iostream>
#include <string>

#include "source/unix_signal_handler.h"

#include "source/server.h"

namespace
{
    const unsigned int READ_WRITE_TIMEOUT_S = 5;
    const int PORT = 12345;
}

UnixQuitSignalHandler* UnixQuitSignalHandler::handler;

int main()
{
    UnixQuitSignalHandler signalHandler([]() // Normal exit by system signal
    {
        exit(0);
    });

    ServerDomain::IoUringServer server(::READ_WRITE_TIMEOUT_S, ::PORT);
    server.work();
}
