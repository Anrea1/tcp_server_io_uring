#ifndef IO_STEPS_H
#define IO_STEPS_H

#include <string>
#include <list>
#include <tuple>
#include <vector>
#include <memory>

#include <arpa/inet.h>
#include <liburing.h>

namespace IoSteps
{
    using CqeResType = decltype(io_uring_cqe::res);

    struct IoStepParams
    {
        io_uring& ioRing;
        std::shared_ptr<int> socketFd;
        std::string client;
        unsigned int readWriteTimeout;
        size_t bufferSize;
    };

    class IIoStep;
    using IIoStepPtr = std::shared_ptr<IIoStep>;
    using IIoStepPtrList = std::list<std::shared_ptr<IIoStep>>;

    class IIoStep
    {
    public:
        virtual ~IIoStep() = default;
        // first parameter - next step objects list
        // second parameter - delete this step flag
        virtual std::tuple<IIoStepPtrList, bool> processing(CqeResType res) = 0;

    protected:
        IIoStep() = default;

    private:
        IIoStep(const IIoStep&) = delete;
        IIoStep(IIoStep&&) = delete;
        IIoStep& operator =(const IIoStep&) = delete;
    };

    class AcceptIoStep : public IIoStep
    {
    public:
        explicit AcceptIoStep(const IoStepParams& params);
        ~AcceptIoStep() override = default;
        std::tuple<IIoStepPtrList, bool> processing(CqeResType res) override;

    private:
        void submit();

        IoStepParams params;
        sockaddr_in sockAddr;
        socklen_t sockAddrLen;
    };

    class ReadIoStep : public IIoStep
    {
    public:
        explicit ReadIoStep(const IoStepParams& params);
        ~ReadIoStep() override = default;
        std::tuple<IIoStepPtrList, bool> processing(CqeResType res) override;

    private:
        void submit();

        IoStepParams params;
        std::vector<char> buffer;
    };

    class TimeoutIoStep : public IIoStep
    {
    public:
        explicit TimeoutIoStep(const IoStepParams& params, const std::string& buffer);
        ~TimeoutIoStep() override = default;
        std::tuple<IIoStepPtrList, bool> processing(CqeResType res) override;

    private:
        IoStepParams params;
        std::string buffer;
        __kernel_timespec timeout;
    };

    class WriteIoStep : public IIoStep
    {
    public:
        explicit WriteIoStep(const IoStepParams& params, const std::string& buffer);
        ~WriteIoStep() override = default;
        std::tuple<IIoStepPtrList, bool> processing(CqeResType res) override;

    private:
        IoStepParams params;
        std::string buffer;
    };
}

#endif //IO_STEPS_H
