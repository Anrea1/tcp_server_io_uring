#include <sys/socket.h>
#include <cstring>
#include <iostream>
#include <map>

#include "server.h"
#include "io_steps.h"

using namespace IoSteps;
using namespace ServerDomain;

class IoUringServer::Impl
{
public:
    Impl(unsigned int readWriteTimeout, int port,
         int queueSize, size_t bufferSize);

    const unsigned int readWriteTimeout;
    const size_t bufferSize;
    int socketFd;
    io_uring ioRing;

    std::map<IIoStep*, IIoStepPtr> stepMap;

private:
    void listenSocket(int port);
};

IoUringServer::IoUringServer(unsigned int readWriteTimeout, int port,
                             int queueSize, size_t bufferSize) :
    d(std::make_unique<Impl>(readWriteTimeout, port, queueSize, bufferSize))
{
}

IoUringServer::~IoUringServer()
{
}

bool IoUringServer::isValid() const
{
    return d->socketFd >= 0;
}

void IoUringServer::work() const
{
    io_uring_cqe* cqe;

    auto acceptStep = std::make_shared<AcceptIoStep>(IoStepParams{ d->ioRing, std::make_shared<int>(d->socketFd), {},
                                                                   d->readWriteTimeout, d->bufferSize });
    d->stepMap[acceptStep.get()] = acceptStep;

    while(true)
    {
        int err = io_uring_wait_cqe(&d->ioRing, &cqe);
        if (err < 0)
        {
            std::cerr << "io_uring_wait_cqe error: " << strerror(errno) << std::endl;
            break;
        }

        auto ioStepPtr = reinterpret_cast<IIoStep*>(cqe->user_data);
        IIoStepPtrList newSteps;
        bool deleteStep;
        std::tie(newSteps, deleteStep) = ioStepPtr->processing(cqe->res);
        if(!newSteps.empty())
        {
            for(const auto& step: newSteps)
            {
                if(step) d->stepMap[step.get()] = step;
            }
        }
        if(deleteStep)
        {
            d->stepMap.erase(ioStepPtr);
        }
        io_uring_cqe_seen(&d->ioRing, cqe);
    }
}

IoUringServer::Impl::Impl(unsigned int readWriteTimeout, int port,
                          int queueSize, size_t bufferSize) :
    readWriteTimeout(readWriteTimeout), bufferSize(bufferSize)
{
    io_uring_queue_init(queueSize, &ioRing, 0);
    this->listenSocket(port);
}

void IoUringServer::Impl::listenSocket(int port)
{
    socketFd = socket(AF_INET, SOCK_STREAM, 0);

    sockaddr_in server_addr;
    server_addr.sin_family = AF_INET;
    server_addr.sin_addr.s_addr = htonl(INADDR_ANY);
    server_addr.sin_port = htons(port);
    int on = 1;
    setsockopt(socketFd, SOL_SOCKET, SO_REUSEADDR, &on, sizeof(on));

    int err = bind(socketFd, (struct sockaddr *) &server_addr, sizeof(server_addr));
    if (err < 0)
    {
        std::cerr << "bind socket error: " << strerror(errno) << std::endl;
        exit(1);
    }

    err = ::listen(socketFd, 1024);
    if (err < 0)
    {
        std::cerr << "listen socket error: " << strerror(errno) << std::endl;
        socketFd = -1;
        return;
    }

    signal(SIGPIPE, SIG_IGN);
}
