#include "unix_signal_handler.h"

#include <iostream>

UnixQuitSignalHandler::UnixQuitSignalHandler(std::function<void()> callback, std::initializer_list<int> quitSignals)
    :callback{callback}
{

    if(!handler)
    {
        handler = this;

        sigset_t blocking_mask;
        sigemptyset(&blocking_mask);

        for (auto sig : quitSignals)
            sigaddset(&blocking_mask, sig);
        struct sigaction sa;

        sa.sa_handler = UnixQuitSignalHandler::handle;
        sa.sa_mask    = blocking_mask;
        sa.sa_flags   = SA_RESTART;

        for (auto sig : quitSignals)
            sigaction(sig, &sa, nullptr);
    }
}

void UnixQuitSignalHandler::handle(int signal)
{
    std::cout << std::endl << "Signal received: " <<  signal << std::endl;
    if(handler) handler->callback();
}






