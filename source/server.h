#ifndef SERVER_H
#define SERVER_H

#include <memory>

namespace ServerDomain
{
    class IoUringServer
    {
    public:
        explicit IoUringServer(unsigned int readWriteTimeout, int port,
                               int queueSize = 1024, size_t bufferSize = 1024);
        ~IoUringServer();

        bool isValid() const;

        void work() const;

    private:
        IoUringServer(const IoUringServer&) = delete;
        IoUringServer(IoUringServer&&) = delete;
        IoUringServer& operator =(const IoUringServer&) = delete;

        class Impl;
        std::unique_ptr<Impl> d;
    };
}

#endif //SERVER_H
