#include "io_steps.h"

#include <netinet/in.h>
#include <unistd.h>
#include <cstring>
#include <iostream>

IoSteps::AcceptIoStep::AcceptIoStep(const IoStepParams& params) : params(params)
{
    this->submit();
}
void IoSteps::AcceptIoStep::submit()
{
    io_uring_sqe* sqe = io_uring_get_sqe(&this->params.ioRing);
    sockAddrLen = sizeof(sockaddr_storage);
    io_uring_prep_accept(sqe, *this->params.socketFd, (sockaddr *)& sockAddr, &sockAddrLen, 0);
    io_uring_sqe_set_data(sqe, this);
    io_uring_submit(&this->params.ioRing);
}
std::tuple<IoSteps::IIoStepPtrList, bool> IoSteps::AcceptIoStep::processing(CqeResType res)
{
    this->submit();

    if (res >= 0)
    {
        auto readParams = this->params;
        readParams.socketFd = std::make_shared<int>(res);
        readParams.client = std::string(inet_ntoa(sockAddr.sin_addr)) + ":" + std::to_string(ntohs(sockAddr.sin_port));

        std::cout << "socket connection accepted: " << readParams.client << std::endl;

        return { { std::make_shared<ReadIoStep>(readParams) }, false };
    }

    std::cerr << "accept connection failed: " << strerror(-res) << std::endl;
    return { IIoStepPtrList{}, false };
}

IoSteps::ReadIoStep::ReadIoStep(const IoStepParams& params) :
    params(params), buffer(params.bufferSize)
{
    this->submit();
}

void IoSteps::ReadIoStep::submit()
{
    io_uring_sqe *sqe = io_uring_get_sqe(&this->params.ioRing);
    io_uring_prep_read(sqe, *this->params.socketFd, buffer.data(), buffer.size(), 0);
    io_uring_sqe_set_data(sqe, this);
    io_uring_submit(&this->params.ioRing);
}

std::tuple<IoSteps::IIoStepPtrList, bool> IoSteps::ReadIoStep::processing(CqeResType res)
{
    if(res > 0)
    {
        std::string str(buffer.data(), res);

        this->submit();

        std::cout << "client " << params.client << " string read " << str << std::endl;
        return { { std::make_shared<TimeoutIoStep>(params, str) }, false };
    }
    else if(res == 0)
    {
        // Client connection lost
        std::cout << "client connection lost " << params.client << std::endl;
    }
    else
    {
        std::cout << "client string read error " << params.client << " -> " << strerror(-res) << std::endl;
    }
    ::close(*params.socketFd);
    *params.socketFd = -1;
    return { IIoStepPtrList{}, true };
}

IoSteps::TimeoutIoStep::TimeoutIoStep(const IoStepParams& params, const std::string& buffer) :
    params(params), buffer(buffer), timeout{ params.readWriteTimeout, 0 }
{
    io_uring_sqe *sqe = io_uring_get_sqe(&this->params.ioRing);
    io_uring_prep_timeout(sqe, &timeout, 0, 0/*IORING_TIMEOUT_ETIME_SUCCESS*/); // On desktop this argument is invalid, on notebook is accepted
    io_uring_sqe_set_data(sqe, this);
    io_uring_submit(&this->params.ioRing);
}
std::tuple<IoSteps::IIoStepPtrList, bool> IoSteps::TimeoutIoStep::processing(CqeResType res)
{
    if(*params.socketFd >= 0 and (res >= 0 or res == -ETIME))
    {
        std::cout << "timeout expired for " << params.client << std::endl;
        return { { std::make_shared<WriteIoStep>(params, buffer) }, true };
    }

    std::cerr << "timeout error for " << params.client << " -> " << strerror(-res) << std::endl;
    return { IIoStepPtrList{}, true };
}

IoSteps::WriteIoStep::WriteIoStep(const IoStepParams& params, const std::string& buffer) :
    params(params), buffer(buffer)
{
    io_uring_sqe *sqe = io_uring_get_sqe(&this->params.ioRing);
    io_uring_prep_write(sqe, *params.socketFd, buffer.data(), buffer.size(), 0);
    io_uring_sqe_set_data(sqe, this);
    io_uring_submit(&this->params.ioRing);
}
std::tuple<IoSteps::IIoStepPtrList, bool> IoSteps::WriteIoStep::processing(CqeResType res)
{
    if(*params.socketFd >= 0 and res > 0)
    {
        std::cout << "client " << params.client << " string wrote " << buffer << std::endl;
    }
    else
    {
        std::cout << "client string write error " << params.client << " -> " << strerror(-res) << std::endl;
    }
    return { IIoStepPtrList{}, true };
}
