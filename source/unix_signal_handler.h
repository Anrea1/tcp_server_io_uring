#ifndef UNIX_QUIT_SIGNAL_HANDLER_H
#define UNIX_QUIT_SIGNAL_HANDLER_H

#include <functional>
#include <csignal>

class UnixQuitSignalHandler final
{
public:

    explicit UnixQuitSignalHandler(std::function<void()> callback,
                               std::initializer_list<int> quitSignals =
                                   {SIGQUIT, SIGINT, SIGTERM, SIGHUP});
    UnixQuitSignalHandler(const UnixQuitSignalHandler&) = delete;
    UnixQuitSignalHandler& operator=(UnixQuitSignalHandler&) = delete;
private:
     static void handle(int signal);
     static UnixQuitSignalHandler* handler;
     std::function<void()> callback;
};

#endif //UNIX_QUIT_SIGNAL_HANDLER_H
